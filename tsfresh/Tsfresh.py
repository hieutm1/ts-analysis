import tsfresh.feature_extraction.feature_calculators as fc
import pandas as pd
import numpy as np
from pyspark.sql.functions import explode
import pyspark.sql.functions as f
from pyspark.sql import SparkSession
from pyspark.sql.functions import sum, max, col, concat, lit, expr
from pyspark.conf import SparkConf
from pyspark.sql.types import *
from pyspark.context import SparkContext
from pyspark.sql.types import StructType, StructField
import os, sys
import operator 
from functools import reduce
import types
from pyspark.sql import Row

def toFloatList(x):
    listX = list(reduce(operator.concat, x)) 
    for i in listX:
        if type(i) is type("str"):
            listX.remove(i)
    list_of_floats = [float(item) for item in listX]
    return list_of_floats

def getBalance(records):
    listBalance = []
    for i in set(records):
        if i[4] is not None:
            listBalance.append(i[4])
    return listBalance[::-1]
    
os.environ['SPARK_HOME'] = "D:\spark-2.4.5"
sconf = SparkConf() \
    .setMaster("local[*]") \
    .setAppName("tsfresh app") \
    .set("spark.driver.host", "127.0.0.1")
spark = SparkSession.builder.config(conf = sconf).getOrCreate()
df = spark.read.parquet(r"C:\Users\minhh\work-space\Daily-Balance")


rdd = df.rdd.map(lambda row:
    Row(isdn_key = row[0],
        balance  = getBalance(row[1]),
        len_values = len(row[1]),
       )
)
df2 = spark.createDataFrame(rdd)
rddDF = df2.filter(f.col("len_values") > 0).rdd
rdd2 = rddDF.map(lambda row:
    Row(isdn_key=row[1],
         sum_values = float(fc.sum_values(row[0])),
         abs_values = float(fc.abs_energy(row[0])),
         absolute_of_changes = float(fc.absolute_sum_of_changes(row[0])),
         agg_autocorrelation = toFloatList(fc.agg_autocorrelation(row[0], [{"f_agg" : "median", "maxlag" : 2}])),
         approximate_entropy = float(fc.approximate_entropy(row[0], 2, 3)),
         ar_coefficient = toFloatList(fc.ar_coefficient(row[0], [{"coeff" : 2 , "k": 1}, {"coeff" : 3 , "k": 2}])),
         augmented_dickey_fuller = toFloatList(fc.augmented_dickey_fuller(row[0], [{"attr": "pvalue"}])),
         autocorrelation = float(fc.autocorrelation(row[0], 1)),
         binned_entropy = float(fc.binned_entropy(row[0], 5)),
         c3 = float(fc.c3(row[0], 1)),
         cid_ce = float(fc.cid_ce(row[0], False)),
         count_above_mean = float(fc.count_above_mean(row[0])),
         count_below_mean = float(fc.count_below_mean(row[0])),
         fft_coefficient = toFloatList(fc.fft_coefficient(row[0], [{"coeff": 1, "attr": "abs"}])),
         first_location_of_maximum = float(fc.first_location_of_maximum(row[0])),
         first_location_of_minimum = float(fc.first_location_of_minimum(row[0])),
         has_duplicate = float(fc.has_duplicate(row[0])),
         has_duplicate_max = float(fc.has_duplicate_max(row[0])),
         has_duplicate_min = float(fc.has_duplicate_min(row[0])),
         index_mass_quantile = toFloatList(fc.index_mass_quantile(row[0], [{"q": 0.1}, {"q": 0.5}, {"q": 0.9}])),
         kurtosis = float(fc.kurtosis(row[0])),
         large_standard_deviation = float(fc.large_standard_deviation(row[0], 0.2)),
         last_location_of_maximum = float(fc.last_location_of_maximum(row[0])),
         last_location_of_minimum = float(fc.last_location_of_minimum(row[0])),
         length = float(fc.length(row[0])),
         linear_trend = toFloatList(fc.linear_trend(row[0], [{"attr": "pvalue"}])),
         longest_strike_above_mean = float(fc.longest_strike_above_mean(row[0])),
         longest_strike_below_mean = float(fc.longest_strike_below_mean(row[0])),
         maximum = float(fc.maximum(row[0])),
         mean = float(fc.mean(row[0])),
         mean_abs_change = float(fc.mean_abs_change(row[0])),
         mean_change = float(fc.mean_change(row[0])),
         mean_second_derivative_central = float(fc.mean_second_derivative_central(row[0])),
         median = float(fc.median(row[0])),
         minimum = float(fc.minimum(row[0])),
         number_crossing_m = float(fc.number_crossing_m(row[0], 10000.0)),
         number_cwt_peaks = float(fc.number_cwt_peaks(row[0], 2)),
         number_peaks = float(fc.number_peaks(row[0],2)),
         partial_autocorrelation = toFloatList(fc.partial_autocorrelation(row[0], [{"lag":1}, {"lag":2}])),
         percentage_of_reoccurring_datapoints_to_all_datapoints = float(fc.percentage_of_reoccurring_datapoints_to_all_datapoints(row[0])),
         percentage_of_reoccurring_values_to_all_values = float(fc.percentage_of_reoccurring_values_to_all_values(row[0])),
         quantile = float(fc.quantile(row[0], 0.5)),   
         ratio_beyond_r_sigma = float(fc.ratio_beyond_r_sigma(row[0], 0.3)),
         ratio_value_number_to_time_series_length = float(fc.ratio_value_number_to_time_series_length(row[0])),
         sample_entropy = float(fc.sample_entropy(row[0])),
         skewness = float(fc.skewness(row[0])),
         standard_deviation = float(fc.standard_deviation(row[0])),
         sum_of_reoccurring_data_points = float(fc.sum_of_reoccurring_data_points(row[0])),
         sum_of_reoccurring_values = float(fc.sum_of_reoccurring_values(row[0])),
         time_reversal_asymmetry_statistic = float(fc.time_reversal_asymmetry_statistic(row[0], 1)),
         variance = float(fc.variance(row[0])),
         variance_larger_than_standard_deviation = float(fc.variance_larger_than_standard_deviation(row[0]))
       )
)
df3 = spark.createDataFrame(rdd2)
for i in df3.schema.fields:
    if(isinstance(i.dataType, ArrayType)):
        nameColumn = i.name
        length = len(df3.select(i.name).take(1)[0][0])
        df3 = df3.select(df3.columns+[expr(nameColumn+'[' + str(x) + ']') for x in range(0, length)]).drop(nameColumn)
df3.show()